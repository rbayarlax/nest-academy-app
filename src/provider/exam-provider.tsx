import React, { createContext, useState } from 'react';

export const ExamContext = createContext<any>({
    score: 0,
    setScore: () => { },
});

export const ExamProvider = ({ children }: any) => {
    const [score, setScore] = useState(0);
  
  return (
    <ExamContext.Provider
      value={{
          score,
          setScore
      }}
    >
      {children}
    </ExamContext.Provider>
  );
};
