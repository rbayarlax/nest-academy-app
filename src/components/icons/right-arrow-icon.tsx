import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const RightArrowIcon: React.FC<IconType> = ({
  role = 'white',
  width = 7,
  height = 10,
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 7 10" fill="none">
      <Path
        d="M3.672 5.005L.842 2.177 2.258.762 6.5 5.005 2.257 9.248.843 7.833l2.829-2.828z"
        fill={colors[role]}
      />
    </Svg>
  );
};
