import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const FillCircleIcon: React.FC<IconType> = ({
  height = 20,
  width = 20,
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 20 20" fill="none">
      <Path
        d="M10 20c5.523 0 10-4.477 10-10S15.523 0 10 0 0 4.477 0 10s4.477 10 10 10z"
        fill="#E8E8E8"
      />
    </Svg>
  );
};
