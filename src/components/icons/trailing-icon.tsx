import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
  width?: number | string;
  height?: number | string;
  color?: string;
}

export const TrailingIcon: React.FC<Props> = ({
    width = 24,
    height = 24,
    color = '#FAFAFA'
}) => {
    return (
        <Svg
      width={width}
      height={height}
      viewBox="0 0 25 24"
      fill="#FFFFFF"
    >
      <Path
        d="M12.672 12l-2.83-2.828 1.415-1.415L15.5 12l-4.243 4.243-1.414-1.415L12.672 12z"
        fill={color}
      />
    </Svg>
    )
}
