import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const ShareIcon: React.FC<IconType> = ({ width = 18, height = 14 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 18 14" fill="none">
      <Path
        d="M8.833 9.167H7.167a7.499 7.499 0 00-6.64 4.008 8.333 8.333 0 018.307-9.008V0l8.333 6.667-8.334 6.666V9.167z"
        fill="#172B4D"
      />
    </Svg>
  );
};
