import * as React from 'react';
import Svg, { Circle, Path, Mask } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const QuestionMarkIcon: React.FC<IconType> = ({
  width = 24,
  height = 24,
  role = 'primary500',
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 4c2.757 0 5 2.243 5 5a5.007 5.007 0 01-4 4.898V15a1 1 0 11-2 0v-2a1 1 0 011-1c1.654 0 3-1.346 3-3s-1.346-3-3-3-3 1.346-3 3a1 1 0 11-2 0c0-2.757 2.243-5 5-5zm-1 15a1 1 0 112 0 1 1 0 11-2 0z"
        fill="#fff"
      />
      <Mask maskUnits="userSpaceOnUse" x={7} y={4} width={10} height={16}>
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12 4c2.757 0 5 2.243 5 5a5.007 5.007 0 01-4 4.898V15a1 1 0 11-2 0v-2a1 1 0 011-1c1.654 0 3-1.346 3-3s-1.346-3-3-3-3 1.346-3 3a1 1 0 11-2 0c0-2.757 2.243-5 5-5zm-1 15a1 1 0 112 0 1 1 0 11-2 0z"
          fill="#fff"
        />
      </Mask>
    </Svg>
  );
};
