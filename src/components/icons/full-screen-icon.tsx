import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const FullScreenIcon: React.FC<IconType> = ({
  height = 21,
  width = 21,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 21 21" fill="none">
      <Path
        d="M14 2.625h5.25v5.25H17.5v-3.5H14v-1.75zm-12.25 0H7v1.75H3.5v3.5H1.75v-5.25zm15.75 14v-3.5h1.75v5.25H14v-1.75h3.5zm-14 0H7v1.75H1.75v-5.25H3.5v3.5z"
        fill="#FAFAFA"
      />
    </Svg>
  );
};
