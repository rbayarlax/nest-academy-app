import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const LeftControlIcon: React.FC<IconType> = ({
  height = 49,
  width = 49,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 49 49" fill="none">
      <Path
        d="M16.333 23.138L37.205 9.224a1.02 1.02 0 011.587.85v28.852a1.021 1.021 0 01-1.587.85L16.333 25.862v12.93a2.042 2.042 0 11-4.083 0V10.207a2.042 2.042 0 114.083 0v12.93zm18.375 10.065V15.796L21.654 24.5l13.054 8.703z"
        fill="#FAFAFA"
      />
    </Svg>
  );
};
