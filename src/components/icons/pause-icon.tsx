import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const PauseIcon: React.FC<IconType> = ({ height = 49, width = 49 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 49 49" fill="none">
      <Path
        d="M24.5 44.917c-11.276 0-20.417-9.14-20.417-20.417 0-11.276 9.14-20.416 20.417-20.416 11.276 0 20.417 9.14 20.417 20.416S35.777 44.917 24.5 44.917zm0-4.084a16.333 16.333 0 100-32.666 16.333 16.333 0 000 32.666zm-6.125-22.458h4.083v12.25h-4.083v-12.25zm8.167 0h4.083v12.25h-4.083v-12.25z"
        fill="#FAFAFA"
      />
    </Svg>
  );
};
