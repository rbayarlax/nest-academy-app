import React from 'react';
import Svg, { Path } from 'react-native-svg';

export const Check: React.FC<any> = ({ width = 16, height = 12 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 16 12" fill="none">
      <Path
        d="M6.16667 8.90739L14.5927 0.480469L15.8898 1.77664L6.16667 11.4997L0.333008 5.66605L1.62917 4.36989L6.16667 8.90739Z"
        fill="#172B4D"
      />
    </Svg>
  );
};
