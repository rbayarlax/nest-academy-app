import React from 'react';
import { Border, Shadow } from './core';
import { Spacing, Box, Stack } from './layout';

export const AdmissionProcessCardHeader: React.FC<any> = ({ children}) => {
  return (
    <Box flexDirection={'column'} height={'auto'}>
      <Stack size={2}>{children}</Stack>
      <Box width={'100%'} height={1} role={'primary200'}></Box>
    </Box>
  );
};

export const AdmissionProcessCardContent: React.FC<any> = ({ children, padding = 0  }) => {
  return (
    <Spacing p={padding} >
      <Box flexDirection={'column'} height={'auto'}>
        <Stack size={2}>{children}</Stack>
      </Box>
    </Spacing>
  );
};

export const AdmissionProcessCard: React.FC<any> = ({ children, padding = 0}) => {
  return (
    <Box width={'100%'} alignSelf={'flex-start'} height={'auto'}>
      <Shadow h={2} w={0} radius={4} opacity={0.25} role={'primary500'}>
        <Shadow radius={2} h={0} w={0} opacity={0.25} role={'primary500'}>
          <Border radius={4}>
            <Box width={'100%'} height={'auto'} role={'white'}>
              <Spacing p={padding} >
                <Box width={'100%'} height={'auto'}>
                  {children}
                </Box>
              </Spacing>
            </Box>
          </Border>
        </Shadow>
      </Shadow>
    </Box>
  );
};

type AdmissionProcessCard = {
  children: any;
  padding?: number;
}