import React, { useEffect, useRef } from 'react';
import { Dimensions, Animated } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from '../core';
import { Box } from '../layout';
const { width } = Dimensions.get('window');

interface Props {
  state: any;
  navigation: any;
  currentScreenIndex: number;
  setCurrentScreenIndex: Function;
}

export const StickyTopBar: React.FC<Props> = ({
  state,
  navigation,
  currentScreenIndex,
  setCurrentScreenIndex,
}) => {
  const xVal = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.spring(xVal, {
      toValue: currentScreenIndex,
      velocity: 0.1,
      useNativeDriver: true,
    }).start();
  }, [currentScreenIndex]);

  return (
    <Box role={'white'}>
      {/* ene n bolohoor yg deed taliin topTab n yvj bgn */}
      <Box height={49} width={width} flexDirection="row" alignItems={'center'}>
        {state.map((route: any, index: any) => {
          const label = state[index].name;
          const isFocused = state.index === index;

          const onPress = () => {
            setCurrentScreenIndex(index);

            if (currentScreenIndex !== index) {
              navigation.navigate(route.name);
            }
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              onPress={onPress}
              style={{
                width: width / state.length,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              key={route.name}
            >
              <Text
                bold={currentScreenIndex == index ? true : false}
                textAlign="center"
              >
                {label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </Box>

      {/* dood taliin guudeg animation */}
      <Box height={2} width="100%">
        <Box height={2} width="100%" role="gray" position={'absolute'} />
        <Animated.View
          style={[
            {
              height: 2,
              width: width / state.length,
              backgroundColor: '#172B4D',
              position: 'absolute',
            },
            {
              transform: [
                {
                  translateX: Animated.multiply(width / state.length, xVal),
                },
              ],
            },
          ]}
        />
      </Box>
    </Box>
  );
};
