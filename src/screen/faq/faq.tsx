import React, { useState } from 'react';
import { SafeAreaView, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {
  Collapse,
  Spacing,
  Text,
  LeftArrowIcon,
  Box,
  Button,
} from '../../components';
import { useQuery } from '@apollo/client';
import { Request_FAQ_ANSWER_VIDEOS } from '../../graphql';

const faqs = [
  {
    title: 'Танай хаяг хаана вэ?',
    body:
      'Манай хаяг Сүхбаатар дүүрэг, Blue Sky зочид буудлын чанх урд , Мэрү тауар, 504 тоот.',
  },
  {
    title: 'Хэдэн насны хүүхэд аль хөтөлбөрт хамрагдаж болох вэ?',
    body:
      'Бид 7-12- ангийхан буюу 14-17 насныханд зориулсан үндсэн хөтөлбөр болон 18-с дээш насныханд зориулсан Leap хөтөлбөрүүдээ танд санал болгож байна.',
  },
  {
    title: 'Элсэлтэнд яаж бүртгүүлэх вэ?',
    body:
      'Манай бүх хөтөлбөрийн элсэлт жилийн турш цахимаар явагддаг. Та nestacademy.mn хаягаар эсвэл ЯГ одоо манай нүүр хуудаснаас swipe up хийгээд хүссэн хөтөлбөр, элсэлт боломжит улирлаа сонгон бүртгүүлэх боломжтой.',
  },
  {
    title: 'Танай сургалтын орчинтой танилцаж болох уу?',
    body:
      'Үндсэн хөтөлбөрийн элсэлт бүртгэл, шалгалт ярилцлага гэсэн 3 хэсгээс бүрдэр бөгөөд Leap хөтөлбөр бүртгэл, ярилцлага гэсэн 2 хэсгээс бүрднэ. Та яг одоо манай нүүр хуудаснаас swipe up хийж бүртгүүлэн цахимаах элсэлтийн шалгалт өгөх боломжтой.',
  },
  {
    title: 'Leap хөтөлбөр гэж юу вэ?',
    body:
      'Бид үйл ажиллагаа сургалтын байраа тэлсээр байгаа бөгөөд сурагч бүрт тав тухтай чөлөөт орчинд Mac үйлдлийн систем дээр суралцах, дадлагажих боломж олгодог. Мөн бид сурагчддаа амт чанартай, эрүүл өдрийн хоолоор үйлчилж байна.',
  },
  {
    title: 'Үндсэн хөтөлбөртэй танилцаж болох уу?',
    body:
      '18-с дээш насны хүн бүхэн Leap хөтөлбөрт хамрагдах боломжтой бөгөөд богино хугацаанд өндөр ур чадвартай болон мөн дадлага, ажлын байр, цаашлаад гадаадын их сургуульд, зуучлуулах боломжтой. Та нийт 16 сарын дотор шинэ мэргэжил, туршлага хуримтлуулах эсвэл ажил мэргэжлээ бүрэн солихыг хүсвэл тус хөтөлбөрийг сонгоорой.',
  },
];

export const FaqScreen = () => {
  const navigation = useNavigation();
  const [indexOfOpenedQuestion, setIndexOfOpenedQuestion] = useState(null);
  const { data, error, loading } = useQuery(Request_FAQ_ANSWER_VIDEOS);
  const videoUrls =
    data &&
    data.frequentlyAskedQuestionsFaqCollection.items[0].answerVideosCollection
      .items;

  return (
    <Box flex={1} role={'white'}>
      {loading ? (
        <></>
      ) : (
        <Spacing p={4}>
          <FlatList
            data={faqs}
            renderItem={({ item, index }: any) => (
              <Collapse
                index={index}
                body={item.body}
                title={item.title}
                url={videoUrls && videoUrls[index].url}
                indexOfOpenedQuestion={indexOfOpenedQuestion}
                setIndexOfOpenedQuestion={setIndexOfOpenedQuestion}
              />
            )}
            keyExtractor={(item, index) => 'faq' + index}
            showsHorizontalScrollIndicator={false}
            ItemSeparatorComponent={() => <Spacing mt={3} />}
          />
        </Spacing>
      )}
    </Box>
  );
};
