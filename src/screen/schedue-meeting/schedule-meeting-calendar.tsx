import React from 'react';
import { Banner, Border, Shadow, Text } from '../../components';
import { Box, Spacing, Stack } from '../../components/layout';
import { Calendar } from 'react-native-calendars';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useNavigation } from '@react-navigation/native';

export const ScheduleMeetingCalendarScreen = () => {
  const datesMarked = {
    '2021-05-09': { marked: true, dotColor: '#4FB83D' },
    '2021-05-10': { marked: true, dotColor: '#4FB83D' },
    '2021-05-11': { marked: true, dotColor: '#4FB83D' },
    '2021-05-12': { marked: true, dotColor: '#4FB83D' },
  };
  const navigation = useNavigation();
  return (
    <Spacing grow={1} ml={4} mr={4} mt={12}>
      <Stack size={4}>
        <Spacing mb={2}>
          <Box width={'100%'} height={129}>
            <Banner type={'info'} title={'Мэдэгдэл'}>
              <Text numberOfLines={3} type={'callout'}>
                Та манай мастеруудтай 30 мин ярилцлага хийснээр элсэлтэнд бүрэн
                хамрагдана.
              </Text>
            </Banner>
          </Box>
        </Spacing>
        <Text
          role={'primary500'}
          bold
          type={'headline'}
          fontFamily={'Montserrat'}
        >
          Та боломжит өдрөө сонгоно уу
        </Text>
        <Shadow w={2}>
          <Border radius={4}>
            <Calendar
              theme={{
                arrowColor: 'black',
              }}
              onDayPress={(pickedDay) => {
                const checkDay = Object.keys(datesMarked).find(
                  (cur) => cur === pickedDay.dateString
                );
                if (checkDay) {
                  navigation.navigate(NavigationRoutes.ScheduleMeetingTime, {
                    pickedDay,
                  });
                }
              }}
              markedDates={datesMarked}
            ></Calendar>
          </Border>
        </Shadow>
      </Stack>
    </Spacing>
  );
};
