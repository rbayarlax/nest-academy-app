import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { TopBarNavigator, TopBarScreen } from '../../components';
import { FCAboutScreen, FCStudyScreen } from './index';
import { NavigationContainer } from '@react-navigation/native';
import { Box } from '../../components/layout';

export const FreeCourseScreen = () => {
  const Header = () => {
    return (
      <Image
        style={styles.img}
        source={require('../../assets/images/free-course-img.png')}
      />
    );
  };

  return (
    <TopBarNavigator Header={Header}>
      <TopBarScreen name={'Тухай'} component={FCAboutScreen} />
      <TopBarScreen name={'Хичээлүүд'} component={FCStudyScreen} />
    </TopBarNavigator>
  );
};
const styles = StyleSheet.create({
  img: {
    width: '100%',
    height: 256,
  },
});
