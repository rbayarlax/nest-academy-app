import React from 'react';
import { KeyboardAvoidingView, Platform } from 'react-native';
import {
  PersonalInformationHop,
  PersonalInformationLeap,
} from '../personal-information';
import auth from '@react-native-firebase/auth';
import { useDocument } from '../../hooks';
import { EmptyScreen } from '../personal-information/empty-screen';

export const PersonalInformationScreen = () => {
  const user: any = auth().currentUser;
  const userAge: any = useDocument(`users/${user.uid}`);
  if (userAge.doc != null) {
    if (userAge.doc.age >= 18) {
      return (
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        >
          <PersonalInformationLeap />
        </KeyboardAvoidingView>
      );
    } else {
      return (
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        >
          <PersonalInformationHop />
        </KeyboardAvoidingView>
      );
    }
  } else {
    return <EmptyScreen></EmptyScreen>;
  }
};
