import React, { useState, useContext, useEffect } from 'react';
import { Text, Box, Button, Spacing } from '../../components';
import { ExamCard } from './examCard';
import { FlatList } from 'react-native';
import { RightArrowIcon } from '../../components/icons/right-arrow-icon';
import { useDocument } from '../../hooks';
import { useNavigation } from '@react-navigation/native';
import { useQuery } from '@apollo/client';
import { REQUEST_EXAM_SCREEN } from '../../graphql/queries';
import _ from 'lodash';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { ExamContext } from '../../provider/exam-provider';

export const ExamScreen: React.FC<any> = (props) => {
  const { data, loading } = useQuery(REQUEST_EXAM_SCREEN);
  const { doc: examScore, updateRecord }: any = useDocument('examScore/1');
  const navigation = useNavigation();

  if (examScore?.score) {
    navigation.navigate(NavigationRoutes.ExamResultScreen, {
      score: examScore?.score,
      totalScore: 30,
    });
  }
  const { score, setScore } = useContext(ExamContext);
  const [quizes, setQuizes] = useState<
    Array<{
      id: string;
      question: string;
      questionPicture: string;
      answerPicture: string;
      correctAnswers: string;
      point: number;
      answers: any;
      type: string;
    }>
  >([]);

  const iqQuizes = data?.exam?.iqCollection.items;
  const designQuizes = data?.exam?.designCollection.items;
  const logicQuizes = data?.exam?.logicCollection.items;
  const mathQuizes = data?.exam?.mathCollection.items;

  const quizItems: any = _.concat(
    _.sampleSize(logicQuizes, 6),
    _.sampleSize(mathQuizes, 13),
    _.sampleSize(designQuizes, 8),
    _.sampleSize(iqQuizes, 3)
  );

  useEffect(() => {
    if (!loading && data) {
      setQuizes(
        _.map(quizItems, (e) => {
          let string = _.map(e.question.json.content, (line) => line.content[0].value).join('\n');
          return {
            id: e?.sys?.id,
            question: string,
            questionPicture: e?.questionPicture?.url,
            answerPicture: e?.answerPicture?.url,
            correctAnswers: e.answer.correctAnswer,
            point: e.point,
            answers: _.sampleSize(e?.answer?.answers, e?.answer?.answers?.length + 2),
            type: e.type
          };
        })
      );
    }
  }, [data]);


  useEffect(() => {
    return () => setScore(0);
  }, []);

  console.log(quizes);

  return (
    <Box flex={1}>
      <Spacing mr={4} ml={4}>
        <FlatList
          style={{ overflow: 'visible' }}
          data={quizes}
          renderItem={({ item, index }) => {
            if (item.type == 'fill') {
              return (
                <ExamCard
                  type={'shortanswerquiz'}
                  imageSrc={null}
                  answer={null}
                  correctAns={item.correctAnswers}
                  qId={index + 1}
                  question={item.question}
                />
              );
            }
            if (item.answerPicture && item.questionPicture) {
              return (
                <ExamCard
                  type={'graphicquizwithimage'}
                  imageSrc={item.questionPicture}
                  answer={item.answerPicture}
                  correctAns={item.correctAnswers}
                  qId={index + 1}
                  question={item.question}
                />
              );
            }
            if (item.answerPicture || item.questionPicture) {
              return (
                <ExamCard
                  type={'graphicquiz'}
                  imageSrc={item.questionPicture}
                  answer={item.answers}
                  correctAns={item.correctAnswers}
                  qId={index + 1}
                  question={item.question}
                />
              );
            }
            if (item.answers) {
              return (
                <ExamCard
                  type={'textquiz'}
                  imageSrc={null}
                  answer={item.answers}
                  correctAns={item.correctAnswers}
                  qId={index + 1}
                  question={item.question}
                />
              );
            }
            return (
              <></>
            )
          }}
          ItemSeparatorComponent={() => <Spacing mt={8} />}
          ListHeaderComponent={() => <Spacing mt={8} />}
          ListFooterComponent={() => <Spacing mb={30} />}
          keyExtractor={(item, index) => item.id}
          showsVerticalScrollIndicator={false}
        />
      </Spacing>
      <Box width={'100%'} height={'auto'} position={'absolute'} bottom={0} role={'lightgray'}>
        <Spacing mb={10} ml={4} mr={4}>
          <Button
            width={'100%'}
            onPress={() => {
              navigation.navigate(NavigationRoutes.ExamResultScreen, {
                score: score && score,
                totalScore: 30,
              });
              console.log(score)
              updateRecord({
                maxScore: 30,
                score: score,
              });
            }}
          >
            <Box width={'auto'} flexDirection={'row'} alignItems={'center'}>
              <Text
                fontFamily={'Montserrat'}
                type={'callout'}
                width={'auto'}
                role={'white'}
                bold
              >
                ШАЛГАЛТ ДУУСГАХ
                </Text>
              <Spacing ml={4}>
                <RightArrowIcon />
              </Spacing>
            </Box>
          </Button>
        </Spacing>
      </Box>
      <Box width={100} height={1}></Box>
    </Box>
  );
};
