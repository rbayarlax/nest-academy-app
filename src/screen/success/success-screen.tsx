import React from 'react';
import { SafeAreaView } from 'react-native';
import {
  Text,
  SuccessIllustration,
  Button,
  Stack,
  Spacing,
  Box
} from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useNavigation } from '@react-navigation/native';

export const SuccessScreen: React.FC<any> = ({ route }) => {
  const { description, title, onPress, icon
  } = route.params || {};
  const navigation = useNavigation();

  return (
      <Stack role='white' height='100%' size={8} justifyContent='flex-start' alignItems='center'>
        <Spacing mt={4} />
        {icon || <SuccessIllustration />}
        <Stack size={4} justifyContent='center' alignItems='center'>
          
          <Text
            bold
            fontFamily={'Montserrat'}
            type={'title3'}
            textAlign='center'
          >
            {title || 'Амжилттай!'}
          </Text>
          <Text textAlign={'center'} width={240} type={'body'}>
            {description || 'Таны бүртгэл амжилттай үүслээ. Та манай аппликэшнд тавтай морил.'}
          </Text>
        </Stack>
        <Button
          onPress={() =>
            onPress && onPress() || navigation.navigate(NavigationRoutes.MainRoot)
          }
        >
          <Text
            fontFamily={'Montserrat'}
            role={'white'}
            type={'body'}
            textAlign={'center'}
            bold
          >
            ҮРГЭЛЖЛҮҮЛЭХ
                      </Text>
        </Button>
      </Stack>
  );
};
