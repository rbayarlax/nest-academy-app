import React from 'react';
import { View, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { Svg, Circle, Path } from 'react-native-svg';
import { Box, Text, Border, TimeIcon, Spacing } from '../../components';
import { CurriculmCard } from './curriculum-card';

export const CurriculmScreen: React.FC<any> = ({ curriculm }) => {
  // info
  const cardData = [
    {
      id: 1,
      title: '1-р шат: Үндсэн мэдлэг',
      lessons: [
        {
          lesson: 'Дизайн үйл явцыг сурах',
        },
        {
          lesson: 'Вэб дизайны үндэс',
        },
        {
          lesson: 'Ideating, Sketching, Wireframe',
        },
        {
          lesson: 'Үндсэн Prototype',
        },
        {
          lesson: 'Хариуцлагатай вэб дизайн xD',
        },
      ],
      duration: '8 сар',
      length: '43 хичээл',
    },
    {
      id: 2,
      title: '2-р шат: UX үндэс',
      lessons: [
        {
          lesson: 'Дизайн үйл явцыг сурах',
        },
        {
          lesson: 'Вэб дизайны үндэс',
        },
        {
          lesson: 'Ideating, Sketching, Wireframe',
        },
        {
          lesson: 'Үндсэн Prototype',
        },
        {
          lesson: 'Хариуцлагатай вэб дизайн xD',
        },
      ],
      duration: '8 сар',
      length: '43 хичээл',
    },
    {
      id: 3,
      title: '3-р шат: Дадлага',
      lessons: [
        {
          lesson: 'Дизайн үйл явцыг сурах',
        },
        {
          lesson: 'Вэб дизайны үндэс',
        },
        {
          lesson: 'Ideating, Sketching, Wireframe',
        },
        {
          lesson: 'Үндсэн Prototype',
        },
        {
          lesson: 'Хариуцлагатай вэб дизайн xD',
        },
      ],
      duration: '8 сар',
      length: '43 хичээл',
    },
    {
      id: 4,
      title: '4-р шат: Ажилийн боломж',
      lessons: [
        {
          lesson: 'Дизайн үйл явцыг сурах',
        },
        {
          lesson: 'Вэб дизайны үндэс',
        },
        {
          lesson: 'Ideating, Sketching, Wireframe',
        },
        {
          lesson: 'Үндсэн Prototype',
        },
        {
          lesson: 'Хариуцлагатай вэб дизайн xD',
        },
      ],
      duration: '8 сар',
      length: '43 хичээл',
    },
  ];
  return (
    <Box flex={1} alignItems={'center'}>
      <FlatList
        data={cardData}
        renderItem={({ item }) => (
          <CurriculmCard
            level={item.title}
            lessons={item.lessons}
            duration={item.duration}
            number={item.length}
          />
        )}
        keyExtractor={(item) => item.id}
        ItemSeparatorComponent={() => <Box width={16} />}
      />
    </Box>
  );
};
